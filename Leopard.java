public class Leopard extends Animal {

    public Leopard(Gender gender) {
        super(gender, Diet.CARNIVORE, new Food[]{Food.MEAT}, 104.9f, 4);
    }    
}
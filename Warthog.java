public class Warthog extends Animal {

    public Warthog(Gender gender) {
        super(gender, Diet.OMNIVORE, new Food[]{Food.GRASS, Food.EGGS, Food.FRUIT}, 102.4f, 8);
    }    
}
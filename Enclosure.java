import java.util.ArrayList;

public class Enclosure<T extends Animal> {
    private int maxSize;
    private ArrayList<T> animals;
    private AnimalConstructor<T> animalConstructor;

    public Enclosure(AnimalConstructor animalConstructor, int maxSize) {
        this.maxSize = maxSize;
        this.animalConstructor = animalConstructor;
        animals = new ArrayList<T>();
    }


	public boolean addAnimal(Gender gender) {
        if (animals.size() >= maxSize) {
            return false;
        }

        animals.add(animalConstructor.createAnimal(gender));

        return true;
    }

    public void removeAnimal(T removedAnimal) {
        animals.remove(removedAnimal);
    }

    public int getAnimalCount() {
        return animals.size();
    }

    public ArrayList<T> getAnimals() {
        return animals;
    }

    public boolean isEnclosureFull() {
        return animals.size() >= maxSize;
    }

    @Override
    public String toString() {
        String output = "No animals";

        if (animals.size() > 0) {
            output = String.format("Animal type: %s\nCount: %d", animals.get(0), animals.size());
        }

        return output;
    }
}
public class Elephant extends Animal {
    public Elephant(Gender gender) {
        super(gender, Diet.HERBIVORE, new Food[]{Food.GRASS, Food.FRUIT}, 100.4f, 1);
    } 
}
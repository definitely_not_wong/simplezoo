public class Meerkat extends Animal {

    public Meerkat(Gender gender) {
        super(gender, Diet.OMNIVORE, new Food[]{Food.GRASS, Food.BUGS, Food.EGGS}, 100.9f, 8);
    }    
}
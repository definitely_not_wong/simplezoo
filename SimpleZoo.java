import java.util.ArrayList;

public class SimpleZoo {

    private static Enclosure[] enclosures;

    private static double GENDER_CHANCE = .5;

    public static void main(String args[]) {
        init();
        run();
    }

    private static void run() {
        boolean isZooFull = false;
        while (!isZooFull) {
            isZooFull = true;

            for (Enclosure zooEnclosure : enclosures) {
                int numberOfBabies = 0;

                if (!zooEnclosure.isEnclosureFull()) {
                    System.out.println("not full");
                    isZooFull = false;
                }

                ArrayList<Animal> animals = zooEnclosure.getAnimals();
                ArrayList<Animal> deadAnimals = new ArrayList<Animal>();

                for (Animal animal : animals) {

                    try {
                        numberOfBabies += animal.giveBirth();
                    } catch (WrongGenderException e) {
                        System.out.println(e);
                    }

                    if (!animal.heartbeat()) {
                        deadAnimals.add(animal);
                    }
                }

                for (Animal deadAnimal : deadAnimals) {
                    zooEnclosure.removeAnimal(deadAnimal);
                }

                for (int i = 0; i < numberOfBabies; i++) {
                    zooEnclosure.addAnimal(determineGender());
                }

                System.out.println(zooEnclosure);
                System.out.println("Added " + numberOfBabies + " new babies");
                System.out.println(deadAnimals.size() + " have died\n");
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void init() {
        enclosures = new Enclosure[] { 
            new Enclosure<Warthog>((Gender gender) -> new Warthog(gender), 7),
            new Enclosure<Kangaroo>((Gender gender) -> new Kangaroo(gender), 5),
            new Enclosure<Meerkat>((Gender gender) -> new Meerkat(gender), 100),
            new Enclosure<Leopard>((Gender gender) -> new Leopard(gender), 3),
            new Enclosure<Zebra>((Gender gender) -> new Zebra(gender), 5),
            new Enclosure<Gorilla>((Gender gender) -> new Gorilla(gender), 7),
            new Enclosure<Elephant>((Gender gender) -> new Elephant(gender), 3)
        };

        for (Enclosure zooEnclosure : enclosures) {
            zooEnclosure.addAnimal(Gender.FEMALE);
            zooEnclosure.addAnimal(Gender.MALE);
        }
    }

    private static Gender determineGender() {
        if (Math.random() > GENDER_CHANCE) {
            return Gender.FEMALE;
        }

        return Gender.MALE;
    }
}
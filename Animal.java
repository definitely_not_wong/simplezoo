abstract class Animal {
    private Gender gender;
    private Diet diet;
    private Food[] food;
    private float temperature;
    private int liter;

    private static final double LIFE_CHANCE = .8;
    private static final double BIRTH_CHANCE = .1;

    Animal(Gender gender, Diet diet, Food[] food, float temperature, int liter) {
        this.gender = gender;
        this.diet = diet;
        this.food = food;
        this.temperature = temperature;
        this.liter = liter;
    }

    int giveBirth() throws WrongGenderException {
        if (!canGiveBirth()) {
            return 0;
        }

        if (gender.equals(Gender.MALE)) {
            throw new WrongGenderException();
        }

        return (int)(Math.random() * liter);
    }

    final boolean heartbeat() {
        return Math.random() <= LIFE_CHANCE;
    }

    Gender getGender() {
        return gender;
    }

    Diet getDiet() {
        return diet;
    }

    Food[] getFood() {
        return food;
    }

    float getTemperature() {
        return temperature;
    }

    int getLiter() {
        return liter;
    }

    private final boolean canGiveBirth() {
        return Math.random() <= BIRTH_CHANCE;
    }
}
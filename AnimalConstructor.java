public interface AnimalConstructor <T extends Animal> {
    public T createAnimal (Gender gender);
}
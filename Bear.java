public class Bear extends Animal {
    public Bear(Gender gender) {
        super(gender, Diet.CARNIVORE, new Food[]{Food.MEAT, Food.FISH}, 100.4f, 2);
    } 
}